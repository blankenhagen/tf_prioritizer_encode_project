import pandas as pd
import os
import argparse
import requests, json
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--linkfile')
#parser.add_argument('-o', '--organism')
#parser.add_argument('-a', '--assay')

args = parser.parse_args()
linkfile_path = args.linkfile
#organism = args.organism
#assay = args.assay

root_dir = "/nfs/data3/INSPECTdb/input"

with open(linkfile_path) as f:
    links = f.read().splitlines()

metadata = pd.read_csv(links[0].strip("\""), sep="\t")
#print(metadata)
experiment_names = np.unique(np.array(metadata["Experiment accession"]))

def find_archived_files(experiment_names):
    archived_files = []
    headers = {'accept': 'application/json'}
    for exp_name in experiment_names:
        link = "https://www.encodeproject.org/experiments/" + exp_name + "/"
        response = requests.get(link, headers=headers)
        json_encode = response.json()

        archieved_analyses = []
        for entry in json_encode["analyses"]:
            if entry["status"] == "archived":
                archieved_analyses.append(entry["accession"])
        assay = json_encode["assay_title"]
        #print(assay)
        
        bed_counter = 0
        bam_counter = 0
        for entry in json_encode["files"]:
            #print( entry["file_format"])
            if entry["file_format"] == "bed":
                bed_counter += 1
                ending = ".bed.gz"
            if entry["file_format"] == "bam":
                bam_counter += 1
                ending = ".bam"
            if entry["file_format"] == "tsv":
                ending = ".tsv"

            if not "RNA" in assay:
                if entry["file_format"] == "bam" or (entry["file_format"] == "bed" and entry["file_format_type"] == "narrowPeak"):
                    if entry["output_type"] == "alignments" or entry["output_type"] == "pseudoreplicated peaks":
                        if entry["assembly"] == "GRCh38" or entry["assembly"] == "mm10":
                            if entry["biological_replicates_formatted"] == "Rep 2" or entry["biological_replicates_formatted"] == "Rep 1":
                                #print(entry["analyses"][0]["@id"].split("/")[2])
                                #print(exp_name + "   " + entry["accession"])
                                #print( entry["analyses"][0]["@id"])
                                #print(ending)
                                if entry["status"] == "archived":
                                    archived_files.append(entry["accession"] + ending)
                                else:
                                    if "analyses" in list(entry.keys()) and len(entry["analyses"]) > 0:
                                        entry_analyses = entry["analyses"][0]["@id"].split("/")[2]
                                        if entry_analyses in archieved_analyses:
                                            archived_files.append(entry["accession"] + ending)
            else:
                if entry["file_format"] == "tsv":
                    if entry["output_type"] == "gene quantifications":
                        if entry["assembly"] == "GRCh38" or entry["assembly"] == "mm10":
                            if entry["biological_replicates_formatted"] == "Rep 2" or entry["biological_replicates_formatted"] == "Rep 1":
                                if entry["status"] == "archived":
                                    archived_files.append(entry["accession"] + ending)
                                else:
                                    if "analyses" in list(entry.keys()) and len(entry["analyses"]) > 0:
                                        entry_analyses = entry["analyses"][0]["@id"].split("/")[2]
                                        if entry_analyses in archieved_analyses:
                                            archived_files.append(entry["accession"] + ending)


        #print(bed_counter)
        if not "RNA" in assay:
            if bed_counter == 0 or bam_counter == 0:
                print("missing bed or bam files detected at experiment: " + exp_name)
    return archived_files

def delete_archived_files(root_directory, archived_files):
    for file_name in archived_files:
        for foldername, subfolders, filenames in os.walk(root_directory):
            if file_name in filenames:
                file_path = os.path.join(foldername, file_name)
                try:
                    os.remove(file_path)  # Delete the file
                    #print(f"File '{file_name}' found and deleted at: {file_path}")
                except OSError as e:
                    print(f"Error: {e}")
                    pass

archived_files = find_archived_files(experiment_names)
#print(archived_files)
delete_archived_files(root_dir, archived_files)

