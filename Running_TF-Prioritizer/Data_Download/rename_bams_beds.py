import pandas as pd
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--linkfile')
#parser.add_argument('-o', '--organism')
#parser.add_argument('-a', '--assay')

args = parser.parse_args()
linkfile_path = args.linkfile
#organism = args.organism
#assay = args.assay

with open(linkfile_path) as f:
    links = f.read().splitlines()

metadata = pd.read_csv(links[0].strip("\""), sep="\t")
#print(metadata.columns)
#organism = list(metadata["Biosample organism"])[1].lower().replace(" ", "_")
#print(organism)

assay = list(metadata["Assay"])[1].lower()
if assay == "histone chip-seq":
    assay = "chip-seq"
assays = [assay, assay + "-bams"]

root_dir = "/nfs/data3/INSPECTdb/input"
#start_dir = root_dir + "/" + organism

for organism in ["homo_sapiens/", "mus_musculus/"]:
    start_dir = root_dir + "/" + organism
    for category_dir in ["tissue/", "primary_cell/", "cell_line/"]:
        for assay in assays:
            assay_dir = start_dir + category_dir + assay + "/"
            for sample_dir in  os.listdir(assay_dir):
                if not "rna-seq" in assay:
                    print(sample_dir)
                    for inner_dir in  os.listdir(assay_dir + "/" + sample_dir):
                        path = assay_dir + "/" + sample_dir + "/" + inner_dir + "/"
                        #print(path)
                        for filename in os.listdir(path):
                            encode_name = filename.split(".")[0]
                            #print(encode_name)
                            #print( metadata.loc[metadata['File accession'] == encode_name])
                            if encode_name in list(metadata['File accession']):
                                experiment_id =  metadata.loc[metadata['File accession'] == encode_name, 'Experiment accession'].iloc[0]
                                Isogenic_replicate =  metadata.loc[metadata['File accession'] == encode_name, 'Biological replicate(s)'].iloc[0]
                                name_new = experiment_id + "_" + str(Isogenic_replicate) + "." + ".".join(filename.split(".")[1:])
                                os.rename(path + filename, path + name_new) 
                            else:
                                print("Could not find file " + filename + " in metadata file")
