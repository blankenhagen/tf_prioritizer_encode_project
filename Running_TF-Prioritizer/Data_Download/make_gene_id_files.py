import os
import pandas as pd
import numpy as np

dir_path = "/nfs/data3/INSPECTdb/input/"

for organism_dir in ["homo_sapiens/"]: 
    #for organism_dir in ["mus_musculus/", "homo_sapiens/"]:
    ids_sample = []
    dfs = []
    names = []

    if organism_dir == "homo_sapiens/":
        ensamle_id = "ENSG"
    if organism_dir == "mus_musculus/":
        ensamle_id = "ENSMUSG"

    for category_dir in ["tissue/", "primary_cell/", "cell_line/"]:
        rna_dir = dir_path + organism_dir + category_dir + "/rna-seq/"
        for sample_dir in os.listdir(rna_dir):
            for filename in os.listdir(rna_dir + sample_dir):
                if filename.endswith('.tsv') and not "count" in filename and not "Gene_id" in filename:
                    file_path = os.path.join(rna_dir+sample_dir, filename)
                    try:
                        # Read the TSV file into a DataFrame
                        df = pd.read_csv(file_path, sep='\t')
                        #target_id       length  eff_length      est_counts      tpm
                        ids_filtered = []                        
                        if "gene_id" in df.columns:
                            #ids_filtered = df[df['gene_id'].str.contains('ENSMUSG', na=False)]['gene_id']
                            ids_filtered = [k for k in list(df['gene_id']) if ensamle_id in k]                            
                        else:
                            #ids = df[df['target_id'].str.contains('ENSMUSG', na=False)]['target_id']
                            ids = [k for k in list(df['target_id']) if ensamle_id in k]
                            ids_filtered = []
                            for id_file in ids:
                                id_split = id_file.split("|")
                                ids_filtered.append(id_split[1])

                        ids_filtered = np.sort(np.unique(np.array(ids_filtered)))
                        
                        #print(ids_filtered)
                        ids_filtered = np.array(ids_filtered).flatten()
                        #print(list(ids_filtered))
                        if len(ids_filtered) > 0:
                            names.append(rna_dir + sample_dir + "/" + filename)
                            dfs.append(df)
                            ids_sample.append(ids_filtered)
                        else:
                            print(df.columns)
                            print(list(df['gene_id']))
                            #print(ids_filtered)
                            #os.remove(rna_dir + sample_dir + "/" + filename)
                            print("no ids: " + rna_dir + sample_dir + "/" + filename)
                    except Exception as e:
                        print('Error reading '+ file_path + ' : {e}')

        
    # intersect ids_sample
    # print(ids_sample)
    if len(ids_sample) > 1:
        intersect = np.intersect1d(ids_sample[0], ids_sample[1])
        for i in range(2, len(ids_sample)):
            intersect = np.intersect1d(intersect, ids_sample[i])
            #print(intersect)
    # save one gene_id.txt file for each organism
    gene_ids = np.sort(np.unique(np.array(intersect)))
    pd.DataFrame({'gene_id': gene_ids}).to_csv(dir_path + organism_dir +"/genome_info/Gene_id.txt", index=False, header=False)
    print(len(gene_ids))
    # over writes the old transcript files with counts only
    for i in range(len(dfs)):

        df = dfs[i]
        if "gene_id" in df.columns:
            transcript_id= "gene_id"
            count = "expected_count"
            #filtered_df = df[df[transcript_id].isin(gene_ids)]
        else:
            transcript_id= "target_id"
            count = "est_counts"
            # in this format ids are very long compositions of several ids seperated by |, we only need the transcript id at the 2nd position
            df['target_id'] = df['target_id'].str.split('|').apply(lambda x: x[1] if len(x) > 1 else '')

        print("Gene ID Length: " + str(len(gene_ids)))
        #filtered_df = df[df[transcript_id].str.contains('|'.join(gene_ids), na=False, regex=True)]
        filtered_df = df[df[transcript_id].isin(gene_ids)]
        #print(filtered_df.shape)
        #print(len(list(filtered_df[count])))
        #print(names[i])
        result_df = filtered_df.groupby(transcript_id)[count].sum().reset_index()
        #print(len(list(result_df[count])))
        result_df = result_df.sort_values(by=transcript_id)
        #print(len(list(result_df[count])))
        #result_dfs.append(result_df)
        #print(result_df[count])
        
        result_df[count].to_csv(names[i].split(".")[0] + "_counts.tsv", index=False, header=False, sep='\t')
        #result_df[count].to_csv(names[i], index=False, header=False, sep='\t')


