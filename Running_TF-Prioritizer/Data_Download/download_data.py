try:
    import requests
except ImportError:
    os.system('python -m pip install requests')
    import requests

try:
    import pandas as pd
except ImportError:
    os.system('python -m pip install pandas')
    import pandas as pd

try:
    import argparse
except ImportError:
    os.system('python -m pip install argparse')
    import argparse

try:
    import multiprocessing
except ImportError:
    os.system('python -m pip install multiprocessing')
    import multiprocessing

try:
    import math
except ImportError:
    os.system('python -m pip install math')
    import math

try:
    import time
except ImportError:
    os.system('python -m pip install time')
    import time

try:
    import os
except ImportError:
    os.system('python -m pip install os')
    import os

#import pandas as pd
#import os
#import argparse
#import multiprocessing
#import math
#import io
#import time
from requests.exceptions import RequestException, HTTPError, ConnectionError
counter = 0

def download_file(url, metadata, callback):
    # you can get incomplete reads if the server looses internet connection, in this case we wait and try again up to three times
    max_retries = 3
    save_path = get_save_path(url, metadata)
    save_dict = "/".join(save_path.split("/")[:-1])
    if os.path.isdir(save_dict):
        if not os.path.exists(save_path):
            for retry in range(max_retries):
                with requests.get(url, stream=True) as r:
                    try:
                        r.raise_for_status()
                        with open(save_path, 'wb') as f:
                            for chunk in r.iter_content(chunk_size=1024 * 1024):
                                if chunk:
                                    f.write(chunk)
                    except (HTTPError, ConnectionError) as e:
                        print(f"Retry {retry + 1}/{max_retries}: {e}")
                        time.sleep(5)  # Wait for a while before retrying
                    except HTTPError as e:
                        if e.response.status_code == 503:
                            print(f"Retry {retry + 1}/{max_retries}: HTTPError 503 - Service Unavailable")
                        else:
                            print(f"HTTPError: {e}")
                            break  # Break the loop for other HTTP errors
                        time.sleep(5)
                    else:
                        callback()
                        print(f"File downloaded successfully: {save_path}")
                        break
        else:
            callback()
            print("file already downloaded: " + save_path)
    else:
        print("Could not find path: " + save_dict)

def get_save_path(url, metadata):
    encode_name = url.split("/")[-1].split(".")[0]

    assay = metadata.loc[metadata['File accession'] == encode_name, 'Assay'].iloc[0].replace(" ", "_").lower()
    sample_name = metadata.loc[metadata['File accession'] == encode_name, 'Biosample term name'].iloc[0].replace(" ", "_").replace("/", "_").replace("\'", "")
    sample_type = metadata.loc[metadata['File accession'] == encode_name, 'Biosample type'].iloc[0].replace(" ", "_").lower()
    organism =  metadata.loc[metadata['File accession'] == encode_name, 'Biosample organism'].iloc[0].replace(" ", "_").lower()
    #print(assay + " " + sample_name + " " + sample_type + " " + organism)
    
    if assay == "total_rna-seq" or assay == "polya_plus_rna-seq":
        assay = "rna-seq"
    if assay == "histone_chip-seq":
        assay = "chip-seq"
    assay_bam = assay
    if assay == "atac-seq" or assay == "dnase-seq" or assay == "chip-seq":
        #experiment_id =  metadata.loc[metadata['File accession'] == encode_name, 'Experiment accession'].iloc[0]
        #Isogenic_replicate =  metadata.loc[metadata['File accession'] == encode_name, 'Biological replicate(s)'].iloc[0]
        #name = experiment_id + "_" + str(Isogenic_replicate) + "." + ".".join(url.split("/")[-1].split(".")[1:])
        if "bam" in url.split("/")[-1]:
            assay_bam = assay + "-bams"
    if assay == "chip-seq":
        assay = metadata.loc[metadata['File accession'] == encode_name,'Experiment target'].iloc[0].split("-")[0]
    #    name = url.split("/")[-1]

    name = url.split("/")[-1].replace("\'", "")

    if assay_bam == "rna-seq":
        save_path = "/nfs/data3/INSPECTdb/input/" + organism + "/" + sample_type + "/" + assay_bam + "/" + sample_name + "/" + name

    else:
        save_path = "/nfs/data3/INSPECTdb/input/" + organism + "/" + sample_type + "/" + assay_bam + "/" + sample_name + "/" + assay + "/" + name
    if not os.path.isdir("/".join(save_path.split("/")[: -1])):
        os.mkdir("/".join(save_path.split("/")[: -1]))
    return save_path

def mp_worker(links_chunk, metadata, length_links, callback):
    for link in links_chunk:
        download_file(link, metadata, callback)


def multiprocess_download(links, metadata, num_processes, callback):
    chunks = int(math.ceil(len(links)/num_processes))
    procs=[]
    for i in range(num_processes):
        proc = multiprocessing.Process(target=mp_worker, args=(links[chunks*i:chunks*(i+1)], metadata, len(links), callback))
        procs.append(proc)
        proc.start()
    #print("File download is finished: downloaded {counter}/{length_links}")

#def log_result(retval):
#    results.append(retval)
#    if len(results) % (len(links)//10) == 0:
#        print('{:.0%} done'.format(len(results)/len(data)))

def progress_callback():
    global counter
    #counter += bytes_downloaded
    counter += 1
    #print(f"(Files downloaded: {counter}/{length_links})")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--linkfile')
    #parser.add_argument('-i', '--index')
    parser.add_argument('-p', '--processes')

    args = parser.parse_args()
    linkfile_path = args.linkfile
    #index = args.index
    processes = args.processes

    with open(linkfile_path) as f:
        links = f.read().splitlines()

    metadata = pd.read_csv(links[0].strip("\""), sep="\t")
    #metadata = metadata[metadata['File assembly'].isin(['mm10', 'hg38'])]
    links = links[1:]
    counter = 0 
    length_links = len(links)
    multiprocess_download(links, metadata, int(processes), progress_callback)
    #print("File download is finished!")

