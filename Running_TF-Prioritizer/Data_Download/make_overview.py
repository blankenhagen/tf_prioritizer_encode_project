import pandas as pd

#link_files = ["./links_mouse_atac.txt", "./links_mouse_dnase.txt", "./links_mouse_chip.txt", "./links_mouse_rna.txt", "./links_human_atac.txt", "./links_human_dnase.txt", "./links_human_chip.txt", "./links_human_rna.txt"]
link_files = ["./download_links_files//human_final_atac.txt","./download_links_files//human_final_dnase.txt", "./download_links_files//human_final_chip.txt", "./download_links_files//human_final_rna.txt"]
metadata_dfs = []

selected_columns = ["File accession", "Biosample term id", "Experiment accession", "File format", "File type",  "Assay", "Biosample organism", "Biosample term name", "Biosample type", "Biological replicate(s)"]
for file in link_files:
    with open(file) as f:
        links = f.read().splitlines()
        metadata = pd.read_csv(links[0].strip("\""), sep="\t")
        #metadata = metadata[metadata['File assembly'].isin(['mm10', 'hg38'])]
        #print(metadata.columns)
        metadata_dfs.append(metadata.loc[:, selected_columns])

result_df = pd.concat(metadata_dfs)
result_df.to_csv("file_overview.csv")

