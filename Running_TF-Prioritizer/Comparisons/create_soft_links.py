
import os

root_dir = "/nfs/data3/INSPECTdb/comparisons/"

for organism_dir in [filename for filename in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir,filename))]:
    current_dir = root_dir +  "/" + organism_dir
    for sample_type_dir in [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
        current_dir = root_dir +  "/" + organism_dir + "/" + sample_type_dir
        for comparison_dir_name in  [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
            current_dir = root_dir +  "/" + organism_dir + "/" + sample_type_dir + "/" + comparison_dir_name
            list_samples = comparison_dir_name.replace("[_", "").replace("_]", "").split("_vs_")
            # get unique samples
            list_samples = list(set(list_samples))
            #print(list_samples)
            for sample in list_samples:
                biosample_name = sample_type_dir
                organism = organism_dir

                for seq_dir in  [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
                    #current_dir = root_dir +  "/" + organism_dir + "/" + sample_type_dir + "/" + comparison_dir_name + "/" + seq_dir
                    link_from = "/nfs/data3/INSPECTdb/input/" + organism + "/" + biosample_name + "/" + seq_dir + "/" + sample
                    link_to = "/nfs/data3/INSPECTdb/comparisons/" + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/" + seq_dir + "/" + sample
                    print(link_from)
                    print(link_to)
                    print("-----------------")
                    if not os.path.islink(link_to):
                        os.symlink(link_from, link_to)



# KOPT-K1_vs_BLaER1_vs_K562_vs_HAP-1_[_K562_vs_HAP-1_]
