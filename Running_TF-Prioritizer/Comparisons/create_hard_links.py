import os
import re

def extract_comparsion_names(input_string):
    # Find all substrings enclosed in brackets and outside brackets
    matches = re.findall(r'\[([^]]+)\]|([^[]+)', input_string)
    # Process the matches
    comps = []
    for match in matches:
        # If the match is enclosed in brackets, add it as a single entry
        if match[0]:
                comps.append(match[0].strip("_").replace("_vs_", "-AND-"))
        # If the match is outside brackets, split it by '_vs_' and add each part as a separate entry
        elif match[1]:
                comps.extend(match[1].split('_vs_'))
    res = []
    for entry in comps:
        if len(entry) > 0:
            res.append(entry)
    return res

root_dir = "/nfs/data3/INSPECTdb/comparisons/"

# interate through all comparisons in the comparisons dictionary
for organism_dir in [filename for filename in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir,filename))]:
    current_dir = root_dir +  "/" + organism_dir
    for sample_type_dir in [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
        current_dir = root_dir +  "/" + organism_dir + "/" + sample_type_dir
        for comparison_dir_name in  [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
            current_dir = root_dir +  "/" + organism_dir + "/" + sample_type_dir + "/" + comparison_dir_name + "/data/"
            # get sample names of comparison
            #list_samples = comparison_dir_name.replace("[_", "").replace("_]", "").split("_vs_")
            list_comp_dirs = extract_comparsion_names(comparison_dir_name)
            #print(list_comp_dirs)
            # get unique samples
            #list_samples = list(set(list_samples))
            for comp in list_comp_dirs:
                sample_list = comp.split("-AND-")
                for sample in sample_list:
                    biosample_name = sample_type_dir
                    organism = organism_dir
                    #print(current_dir)
                    for seq_dir in  [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
                        input_path = "/nfs/data3/INSPECTdb/input/" + organism + "/" + biosample_name + "/" + seq_dir + "/" + sample
                        # in chip-seq the inner folders are not called chip-seq, but have the names of the differnt HMs, this iterates over these folders
                        #print(seq_dir)
                        if  not "no_H" in seq_dir and not "tf-prio" in seq_dir:
                            if not "rna" in seq_dir:
                                for seq_dir_inner in  [filename for filename in os.listdir(input_path) if os.path.isdir(os.path.join(input_path,filename))]:
                                    #print(seq_dir_inner)

                                    file_path_in_input = input_path + "/" + seq_dir_inner
                                    # get the filenames from the input folder
                                    for input_file in  [filename for filename in os.listdir(file_path_in_input) if os.path.isfile(os.path.join(file_path_in_input,filename))]:
                                        link_from = file_path_in_input + "/" + input_file
                                        link_to = root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-") + "/" +  seq_dir_inner + "/" + input_file                        
                                        # make dirs in comparisons that do not exist yet
                                        print(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir)
                                        if not os.path.isdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir):
                                            os.mkdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir)
                                        if not os.path.isdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-")):
                                            os.mkdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-"))
                                        if not os.path.isdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-") + "/" +  seq_dir_inner):
                                            os.mkdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-") +  "/" +  seq_dir_inner)

                                        if not os.path.isfile(link_to):
                                            print(link_from)
                                            print(link_to)
                                            print("-----------------")
                                            os.link(link_from, link_to)
                            else:
                                # get the filenames from the input folder
                                for input_file in  [filename for filename in os.listdir(input_path) if os.path.isfile(os.path.join(input_path,filename))]:
                                    if "counts" in input_file:
                                        link_from = input_path + "/" + input_file
                                        link_to = root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-") + "/" + input_file

                                        # make dirs in comparisons that do not exist yet
                                        if not os.path.isdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir):
                                            os.mkdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir)
                                        if not os.path.isdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-")):
                                            os.mkdir(root_dir + organism + "/" + biosample_name  + "/" + comparison_dir_name + "/data/" + seq_dir + "/" + comp.replace("_", "-"))

                                        if not os.path.isfile(link_to):
                                            print(link_from)
                                            print(link_to)
                                            print("-----------------")
                                            os.link(link_from, link_to)
