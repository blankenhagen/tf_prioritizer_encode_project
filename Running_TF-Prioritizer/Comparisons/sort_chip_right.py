import pandas as pd
import os
import argparse
import requests, json
import numpy as np

root_dir = "/nfs/data3/INSPECTdb/input"

file_overview = pd.read_csv("/nfs/data3/INSPECTdb/input/file_overview.csv")

chip_files = file_overview.loc[file_overview['Assay'] == "Histone ChIP-seq"]
#print(chip_files)
for index, row in chip_files.iterrows():
    if row["File type"] == "bam":
        path = root_dir + "/" + row["Biosample organism"].lower().replace(" ", "_") + "/" + row["Biosample type"].replace(" ", "_") + "/chip-seq-bams/" + row["Biosample term name"].replace(" ", "_")
    else:
        path = root_dir + "/" + row["Biosample organism"].lower().replace(" ", "_") + "/" + row["Biosample type"].replace(" ", "_") + "/chip-seq/" + row["Biosample term name"].replace(" ", "_")
    print(path)
    # what hm does the chip-seq file have?
    link = "https://www.encodeproject.org/experiments/" + row["Experiment accession"] + "/"
    headers = {'accept': 'application/json'}
    response = requests.get(link, headers=headers)
    json_encode = response.json()
    hm_name = json_encode["target"]["label"]
    # sorting the files in folders named after thir hm
    if not os.path.exists(path + "/" + hm_name):
        os.makedirs(path + "/" + hm_name)
    file_name = row["Experiment accession"] + "_" + str(row["Biological replicate(s)"]) + "." + row["File type"]
    if os.path.exists(path + "/chip-seq/" + file_name):
        os.rename(path + "/chip-seq/" + file_name, path + "/" + hm_name + "/" + file_name)

