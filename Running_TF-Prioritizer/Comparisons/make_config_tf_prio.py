import pandas as pd
import os
import json
import numpy as np

#out_file = open("myfile.json", "w")  
    
#json.dump(dict1, out_file, indent = 6)  
    
#out_file.close()  
path_config_ref = "/nfs/data3/INSPECTdb/input/tf_config_reference/"
with open(path_config_ref + "/chipSeq.json", "r") as file:
    config_chip = json.load(file)
    file.close()
with open(path_config_ref + "/atacSeq.json", "r") as file:
    config_atac = json.load(file)
    file.close()
with open(path_config_ref + "/dnaseSeq.json", "r") as file:
    config_dnase = json.load(file)
    file.close()

#print(type(config_dnase))

root_dir = "/nfs/data3/INSPECTdb/comparisons"
input_dir = "/nfs/data3/INSPECTdb/input"
for organism_dir in ["homo_sapiens", "mus_musculus"]:
    #for organism_dir in ["homo_sapiens/"]:
    current_dir = root_dir + "/" + organism_dir
    genome_info_dir = input_dir + "/" + organism_dir + "/genome_info"
    chipAtlas_tissues = pd.read_csv(genome_info_dir + "/chipAtlas_tissues.csv", sep="\t")
    #print(chipAtlas_tissues)
    for sample_type_dir in [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
        current_dir = root_dir + "/" + organism_dir + "/" + sample_type_dir
        for comparison_dir in [filename for filename in os.listdir(current_dir) if os.path.isdir(os.path.join(current_dir,filename))]:
            directory = root_dir + "/" + organism_dir + "/" + sample_type_dir + "/" + comparison_dir
            
            list_configs = [(config_chip, "chip-seq"), (config_atac, "atac-seq"), (config_dnase, "dnase-seq")]
            for config, assay in list_configs:
                # check if data for this assay is availible
                is_data_availible = True
                for sample_dir in [filename for filename in os.listdir( directory + "/data/" + assay + "/") if os.path.isdir(os.path.join( directory + "/data/" + assay + "/",filename))]:
                    if not assay == "chip-seq":
                        if len(os.listdir(directory + "/data/" + assay + "/" + sample_dir + "/" + assay)) == 0:
                            #print(directory + "/" + assay + "/" + sample_dir + "/" + assay + "/")
                            is_data_availible = False
                    else:
                        if len(os.listdir(directory + "/data/" + assay + "/" + sample_dir + "/")) == 0:
                            #print(directory + "/" + assay + "/" + sample_dir + "/" + assay + "/")
                            is_data_availible = False
                
                # config is only made is all samples have data for the assay
                if is_data_availible:
                    # getting states and chipAtlas tissues
                    states_dict = {}
                    tissues = []
                    for state in os.listdir(directory + "/data/" + assay + "/"):
                        states_dict[state] = state
                        split_states = state.split("-AND-")
                        for split_state in split_states:
                            tissue = chipAtlas_tissues.loc[chipAtlas_tissues['sample'] == split_state, 'chipAtlas-tissue'].values
                            if len(tissue) > 0:
                                tissue = tissue[0].replace("_", "-").replace("\'", "").split(",")
                            else:
                                print(state + " not found in chipAtlas_tissue.csv!")
                            tissues.append(tissue)
                    # flatten list and make entries unique
                    tissues = list(np.unique(np.array([item for sublist in tissues for item in sublist])))
                    #print(tissues)

                    config["InputConfigs"]["peaks"] = directory + "/data/" + assay + "/"
                    config["InputConfigs"]["rnaSeq"] = directory + "/data/rna-seq/"
                    config["InputConfigs"]["geneIDs"] = genome_info_dir + "/Gene_id.txt"
                    config["InputConfigs"]["sameStages"] = states_dict
                    if "mus_musculus" in organism_dir:
                        config["InputConfigs"]["geneAnnotationFile"] = genome_info_dir + "/gencode.vM10.annotation.gtf"
                        config["InputConfigs"]["genome"] = "mm10"
                        config["InputConfigs"]["biomartSpecies"] = "mmusculus_gene_ensembl"
                        config["MixOptions"]["blackListPath"] = genome_info_dir + "/mm10-blacklist.v2.txt"
                        config["TEPIC"]["PWMs"] = genome_info_dir + "/combined_Jaspar_Hocomoco_mouse_PSEM.PSEM"
                        config["TEPIC"]["referenceGenome"] = genome_info_dir + "/mm10_genome.fa"
                        config["TEPIC"]["onlyDNasePeaks"] = genome_info_dir + "/gencode.vM10.annotation.gtf"

                    else:
                        config["InputConfigs"]["geneAnnotationFile"] = genome_info_dir + "/gencode.v44.basic.annotation.gtf"
                        config["InputConfigs"]["genome"] = "hg38"
                        config["InputConfigs"]["biomartSpecies"] = "hsapiens_gene_ensembl"
                        config["MixOptions"]["blackListPath"] = genome_info_dir + "/hg38-blacklist.v2.bed"
                        config["TEPIC"]["PWMs"] = genome_info_dir + "/combined_Jaspar_Hocomoco_Kellis_human_PSEM.PSEM"
                        config["TEPIC"]["referenceGenome"] = genome_info_dir + "/hg38_genome.fa"
                        config["TEPIC"]["onlyDNasePeaks"] = genome_info_dir + "/gencode.v44.basic.annotation.gtf"

                    # hint footprinting is only done on atac-seq and dnase-seq data
                    if not assay=="chip-seq":
                        config["HINT"]["bam_directory"] = directory + "/data/" + assay + "-bams/"
                        """
                        if organism_dir == "mus_musculus":
                            config["HINT"]["genome"] = "mm10"
                        else:
                            config["HINT"]["genome"] = "hg38"
                        """
                    config["ChipAtlas"]["tissueTypes"] = tissues

                    #print("saving config " + directory + "  " + assay)
                    with open(directory + "/" + 'config_' + assay +'.json', 'w') as f:
                        json.dump(config, f, indent=2)
                        f.close()

    

