#!/bin/bash
echo -e "sample\tTF\tHM\tTG\tScore" > top_tgs.tsv
for sample_folder in ./*/; do
    sample_name=$(basename "$sample_folder")
    for file in "$sample_folder"/*/*.tsv; do
        histone_mod=$(basename "$(dirname "$file")")
        tf_name=$(basename "$file" .tsv)
        tail -n +2 "$file" | awk -v tf="$tf_name" -v histone="$histone_mod" -v sample="$sample_name" 'BEGIN {OFS="\t"} {print sample, tf, histone, $0}'
    done
done > top_tgs.tsv


