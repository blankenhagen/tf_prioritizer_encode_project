#!/bin/bash
#SBATCH --job-name=alignment_job    # Job name
#SBATCH --ntasks=1                  # Number of tasks (one task for one Python script)
#SBATCH --cpus-per-task=1           # Number of CPU cores per task
#SBATCH --mem=350G                    # Memory per CPU core

#module load python/3.10.1

# Navigate to the directory containing the Python script
cd /nfs/data3/INSPECTdb/analysis/

# Execute the Python script
python3 alignment.py

