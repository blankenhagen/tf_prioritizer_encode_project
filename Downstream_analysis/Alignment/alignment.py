import pandas as pd
import numpy as np
from Bio import pairwise2
from Bio.Align import substitution_matrices
from Bio import Align
from Bio import AlignIO
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from Bio.Seq import Seq

algn_input = pd.read_csv("./mouse_vs_human/alignmant_mouse_human.csv")
from_num = 57
to_num = 59
ensemble_ids = algn_input.iloc[1].tolist()[from_num:to_num] 
dna_seqs_human =  algn_input.iloc[3].tolist()[from_num:to_num]
dna_seqs_mouse =  algn_input.iloc[4].tolist()[from_num:to_num]
protein_seqs_human =  algn_input.iloc[5].tolist()[from_num:to_num]
protein_seqs_mouse =  algn_input.iloc[6].tolist()[from_num:to_num]
TFs = algn_input.iloc[0].tolist()[from_num:to_num]
#print(TFs)
array_species= ["human", "mouse"]
path = "./"

# Define a function to perform pairwise sequence alignment
def align_protein_sequences(seq1, seq2, TF):
    filename = path + "fastas/protein/" +  str(TF) + "_align_protein.fasta"
    score = aligner_protein.score(seq1, seq2)
    #print(score)
    alignments = aligner_protein.align(seq1, seq2)
    with open(filename, "w") as handle:
        for i, record in enumerate(alignments[0]):
            # Create a SeqRecord object with the sequence and sequence name
            seq_record = SeqRecord(Seq(record), id=array_species[i] +"_" + str(TF) + "_" +str(score), description="")
            # Write the SeqRecord to the file in FASTA format
            SeqIO.write(seq_record, handle, "fasta")
    return score

def align_dna_sequences(seq1, seq2, TF):
    filename = path + "fastas/dna/" +  str(TF) + "_align_dna.fasta"
    score = aligner_dna.score(seq1, seq2)
    alignments = aligner_dna.align(seq1, seq2)
    with open(filename, "w") as handle:
        for i, record in enumerate(alignments[0]):
            # Create a SeqRecord object with the sequence and sequence name
            seq_record = SeqRecord(Seq(record), id=array_species[i] +"_" + str(TF) + "_" +str(score), description="")
            # Write the SeqRecord to the file in FASTA format
            SeqIO.write(seq_record, handle, "fasta")
    
    return score


# Define a function to write pairwise alignments to a file
def write_alignment_to_fasta(alignments, filename):
    with open(filename, "w") as handle:
        for j, alignment in enumerate(alignments):
            for i, record in enumerate(alignment):
                # Create a SeqRecord object with the sequence and sequence name
                seq_record = SeqRecord(Seq(record), id=array_species[i] +"_" + TFs[j], description="")
                # Write the SeqRecord to the file in FASTA format
                SeqIO.write(seq_record, handle, "fasta")

aligner_protein = Align.PairwiseAligner()
#aligner_protein.open_gap_score = -10
#aligner_protein.extend_gap_score = -0.5
#aligner_protein.substitution_matrix = extend_matrix_alphabet(substitution_matrices.load("BLOSUM62"))
#print(extend_matrix_alphabet(substitution_matrices.load("BLOSUM62")).alphabet)
aligner_dna = Align.PairwiseAligner()


dna_aligment = []
dna_scores = []
print("dna alignment")
# Iterate over each pair of sequences and perform alignment
for human_seq, mouse_seq, TF, en_id in zip(dna_seqs_human, dna_seqs_mouse, TFs, ensemble_ids):
    if TF == "None" or  TF == "nan":
        TF = en_id
    print(TF)
    score = align_dna_sequences(human_seq, mouse_seq, TF)
    #print(score)
    #dna_scores.append(score)

#print("save score")
#data = {
#    'TF': TFs,
#    'Score': dna_scores,
#}
#df_scores = pd.DataFrame.from_dict(data)
#df_scores.to_csv("./mouse_vs_human/dna_scores.csv")

#np.save("./mouse_vs_human/dna_scores.np", np.array(dna_scores))
print("dna done")

#print("save dna align")
#print(dna_aligment)
#write_alignment_to_fasta(dna_aligment, "./mouse_vs_human/dna_alignments_frist_50.fasta")

"""
protein_aligment = []
protein_scores = []
print("protein aligenment")
# Iterate over each pair of sequences and perform alignment
for human_seq, mouse_seq, TF, en_id in zip(protein_seqs_human, protein_seqs_mouse, TFs, ensemble_ids):
    if None in TF or "None" in TF or  "nan" in TF:
        score = align_protein_sequences(human_seq, mouse_seq, en_id)
    else: 
        score = align_protein_sequences(human_seq, mouse_seq, TF)
    #print(score)
    #protein_aligment.append(alignment[0])
    protein_scores.append(score)
#print("save proetin")
#print(protein_aligment)
#write_alignment_to_fasta(protein_aligment, "./mouse_vs_human/protein_alignments_frist_50.fasta")
data = {
    'TF': TFs,
    'Ensemble_ID': ensemble_ids,
    'Score': protein_scores
}
df_scores = pd.DataFrame.from_dict(data)
df_scores.to_csv("./mouse_vs_human/protein_scores.csv")
print("protein done")
#np.save("./mouse_vs_human/protein_scores.np", np.array(protein_scores))
"""
